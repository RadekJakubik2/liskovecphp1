<?php  
  class Database {
    private $host = 'localhost';
    private $user = 'root';
    private $pass = '123456';
    private $dbname = 'testing';
    private $pdo;
    private $stmt;
    private $error;

    public function __construct(){      
      $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
      $options = array(
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
      );
      
      try{
        $this->pdo = new PDO($dsn, $this->user, $this->pass, $options);
      } catch(PDOException $e){
        $this->error = $e->getMessage();
        echo $this->error;
      }
    }

    public function beginTransaction(){
      $this->pdo->beginTransaction();
    }

    public function commitTransaction(){
      $this->pdo->commit();
    }

    public function rollBackTransaction(){
      $this->pdo->rollBack();
    }
    
    public function prepareQuery($query){
      $this->stmt = $this->pdo->prepare($query);
    }
    
    public function executeWith($array){
      return $this->stmt->execute($array);
    }

    public function execute(){
      return $this->stmt->execute();
    }
    
    public function fetchAll(){
      $this->execute();
      return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

  }
?>