<?php
  define('NoLoadedSQLdata', 'Z databáze se nepodařilo načíst žádná data.');
  define('CountsAreNotEqual','Počet odpovědí a hlasů se u některé otázky v načítaném .xml souboru neshodují!');
  define('FromXmlToSQLError','Při zápisu dat z .xml souboru do databáze došlo k chybě!');
  define('XmlLoadingError','Nepodařilo se správně načíst .xml soubor!');
  define('DataSentOff','Anketa byla odeslána.');
  define('ErrorWhenProcessing','Při zpracování došlo k chybě, anketa nebude zpracována.');
  define('NoAnswerChecked','Alespoň jedna otázka musí být vyplněna!');
  define('AlreadyVoted','Hlasovat lze pouze jednou!');
  define('VotedCookie','isVoted');


