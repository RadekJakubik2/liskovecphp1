<?php
  class Question {
    private $db;
    private $result = '';
    private $url = 'survey.xml';
    //private $url = 'test.xml';

    public function __construct(){
      $this->db = new Database;
    }

    public function getQuestions(){
      $this->db->prepareQuery('SELECT * FROM survey');
      $this->loadData();
      return $this->result;
    }

    public function loadQuestions(){ 
      if(file_exists($this->url))
      {    
        try{
          $data = $this->loadXMLfile();
          $this->insertIntoDb($data);
        }
        catch(XmlDocumentLoadException $e){ }
        catch(LengthException $e){
          $this->db->rollBackTransaction();
          echo CountsAreNotEqual;
        }
        catch(Exception $e){
          $this->db->rollBackTransaction();
          echo FromXmlToSQLError;
        }
      }
    }

    public function updateQuestions(){ 
      $this->extractPostVar();
      $this->extractAnswers();
      if($this->updateDataInDb())
      {
        $cookie_name = VotedCookie;
        $cookie_value = "1";
        setcookie($cookie_name, $cookie_value, time() + (10 * 365 * 24 * 60 * 60), "/");
        echo DataSentOff;        
      }        
    }
    
    private function loadData(){
      $answerId=0;
      $qNum=0;
      $fetchedData = $this->db->fetchAll();
      if($fetchedData){
        foreach ($fetchedData as $row) {
          $this->loadQuestion($answerId,$qNum,$fetchedData,$row);
          $qNum++;
          $answerId=0;
        }
      }
      if($qNum > 0)
        $this->result .= '<input id="submit" type="submit" name="submit" value="Odeslat výsledky" />';
      else
        $this->result = NoLoadedSQLdata;
    }

    private function loadQuestion($answerId, $qNum, $fetchedData, $row){
      $answers = explode("|", $row->answers);
    
      // loading question
      $this->result .= '<label>'.$row->question.'</label> <br>';
      
      // loading answers
      for ($i=0; $i < count($answers); $i++) {
        $value = 'row'.$row->id.'a'.$answerId++;
        $radioButton = '<div>'   
        .'<input type="radio" id='.$value.' name=q'.$qNum.' value='.$value.'>'    
        .'<label for='.$value.'>'.$answers[$i].'</label>'
        .'<div>';
        $this->result .= $radioButton;
      }
    }
    
    private function loadXMLfile(){
      require_once 'XmlDocumentLoadException.php';
        libxml_use_internal_errors(true);
        $data = simplexml_load_file($this->url, 'SimpleXMLElement');
        if($data===false){
          echo XmlLoadingError;
          foreach(libxml_get_errors() as $error) {
            echo "\t", $error->message;
          }
          throw new XmlDocumentLoadException;
        }
        return $data;
    }
    
    private function insertIntoDb($data){
      $query = "
        INSERT INTO survey (question, answers, votes) 
         VALUES(:question, :answers, :votes);";      
      $this->db->beginTransaction();
      $this->db->prepareQuery($query); 
      for($i = 0; $i < count($data); $i++)
      {
        $this->insertDataRow($data, $i);
      }
      $this->db->commitTransaction();
    }

    private function insertDataRow($data, $i){
      $answers = explode("|", $data->point[$i]->answers);
      $votes = explode("|", $data->point[$i]->votes);
      if(!$this->countsAreEqual(count($answers),count($votes))){
        throw new LengthException;
      }
      $this->db->executeWith(
      array(
       ':question'   => $data->point[$i]->question,
       ':answers'  => $data->point[$i]->answers,
       ':votes'  => $data->point[$i]->votes
      )
     );
    }

    private function countsAreEqual($count1, $count2) {
      if($count1 === $count2) return true; 
      else return false;  
    }

    private $dbQuestionIds = array();
    private $answerIndexes = array();
    private $resAnswers = array();  

    function extractPostVar(){
      foreach ($_POST as $key => $value) {
        if(preg_match('/\A[q]\d+\z/', $key, $_POST))
          array_push($this->resAnswers,$value);
      }
    }

    private function extractAnswers(){
      for ($i=0; $i < count($this->resAnswers); $i++) {
        $res = explode("a", $this->resAnswers[$i]);
        array_push($this->dbQuestionIds,substr($res[0],3));
        array_push($this->answerIndexes,$res[1]);
      }
    }
    
    private function getFetchedData(){
      $strIds = implode(",", $this->dbQuestionIds);
      $query = 'SELECT * FROM survey WHERE id IN ('.$strIds.')';
      $this->db->prepareQuery($query);
      return $this->db->fetchAll();
    }
    
    private function getResultString($row, $counter){
      $originVotes = explode("|", $row->votes);
      $voteNumber = intval($originVotes[$this->answerIndexes[$counter]]);
      $originVotes[$this->answerIndexes[$counter]] = ++$voteNumber;
      return implode("|", $originVotes);
    }
  
    private function updateDbRow($resultString, $row){
      $query = "UPDATE survey SET votes=:votes WHERE id=:id";
      $this->db->prepareQuery($query);
      $this->db->executeWith( array('votes' => $resultString, 'id' => $row->id ));
    }
    
    private function updateDataInDb(){
      $processed = true;
      try{
        $this->db->beginTransaction();
        $counter = 0;
        $fetchedData = $this->getFetchedData();
        if($fetchedData){
          foreach ($fetchedData as $row) {
            $resultString = $this->getResultString($row, $counter);
            $this->updateDbRow($resultString, $row);      
            $counter++;
          }
        }
        $this->db->commitTransaction();    
      } 
      catch(Exception $e){    
        $this->db->rollBackTransaction();
        $processed = false;
        if(count($this->dbQuestionIds)==0) echo NoAnswerChecked;
        else echo ErrorWhenProcessing;
      }
      return $processed;
    }
  }
  