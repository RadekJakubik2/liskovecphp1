<?php
  class Questions extends Controller {
    public function __construct(){
      $this->questionModel = $this->model('Question');
    }
    
    public function index(){
      $this->questionModel->loadQuestions();
      $resultHtml = $this->questionModel->getQuestions();
      
      $data = [
        'title' => 'PHP Survey Lískovec',
        'questions' => $resultHtml
      ];
     
      $this->view('surveyView', $data);
    }

    public function edit(){
      if(!isset($_COOKIE[VotedCookie]) && $_SERVER['REQUEST_METHOD'] == 'POST'){
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);        
        $this->questionModel->updateQuestions();
      }
      else
        echo AlreadyVoted;
    }
  }